from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello world!"

@app.route("/about")
def about():
    return "<h1>About Us</h1>"

@app.route("/user/<string:name>")
def user(name):
    return "<h1>Kia ora, "+name+"!</h1>"



app.run(debug=True)